package com.example.pizza;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.MyViewHolder> {
    String titles[],descs[];
    int images[];
    Context context;
    public MyRecyclerViewAdapter(String[] titles, String[] descs, int[] images,Context context) {
        this.titles = titles;
        this.descs = descs;
        this.images = images;
        this.context=context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View root=inflater.inflate(R.layout.row_element, parent,false);
        return new MyViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
      holder.title.setText(titles[position]);
      holder.desc.setText(descs[position]);
      holder.imagepizza.setImageResource(images[position]);
      holder.linearLayout.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              Intent intent=new Intent(context,PizzaActivity.class);
              intent.putExtra("title",titles[position]);
              intent.putExtra("image",images[position]);
              context.startActivity(intent);
          }
      });
    }

    @Override
    public int getItemCount() {
        return images.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title,desc;
        ImageView imagepizza;
        LinearLayout linearLayout;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            title=itemView.findViewById(R.id.titleitem);
            desc=itemView.findViewById(R.id.Descriptionitem);
            imagepizza=itemView.findViewById(R.id.pizaitem);
            linearLayout=itemView.findViewById(R.id.linearpizza);
        }
    }
}
